from flask import Flask, jsonify, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime,timedelta
import time


app = Flask(__name__)
app.config.from_object("project.config.Config")
db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = "User"

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128), nullable=False)
    Pass = db.Column(db.String(128), nullable=False)
    user_email = db.Column(db.String(128), nullable=False)
    # roles = db.relationship('Roles')
    def __init__(self,first_name,last_name, user_email, Pass):
        self.first_name = first_name
        self.last_name =last_name
        self.user_email = user_email
        self.Pass = Pass

# class Roles(db.Model):
#     __tablename__ = "Roles"

#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(100))
#     user_id = db.Column(db.Integer, db.ForeignKey('User.id'))

#     def __init__(self, name):
#        self.name = name


@app.route('/healthCheck', methods = ['GET'])
def api():
    return jsonify( post= "200 OK")


    
@app.route("/", methods=['POST', 'GET'])
def index(): # define the function for the route
    if request.method == 'POST':
        #task content will be equals to whatever the user typed in the box. name same in the one in index.html
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        user_email = request.form['user_email']
        Pass = request.form['Pass']
        new_user = User(first_name = first_name, last_name = last_name, 
        user_email = user_email, Pass =Pass)
        # Roles = User(roles = "user")

        
        try:
            db.session.add(new_user) # adds the new model to the db
            db.session.commit()  #commits those changes 
            return redirect('/')  #go back to the page
        except:
            return 'the  re was an issue adding your task'

    else:
         #users = User.query.order_by(User.date_created).all() #will grab everything in the data base
         return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5000)
